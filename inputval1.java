/*


 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class inputval1 {

    private static String dataFileName = "savdata.xls";
    private static Scanner in;


    public static int ordinalIndexOf(String str, String substr, int n) {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }

    private static String getNamepattern() {
        String pattern = "([a-zA-Z]?\\'?([a-zA-Z]*\\, [a-zA-Z]* [a-zA-Z]*\\.?)|([a-zA-Z]+ [a-zA-Z][a-zA-Z]+)|([a-zA-Z]*, [a-zA-Z]*)|([a-zA-Z]*)|([a-zA-Z]* [a-zA-Z]'[a-zA-Z]*-[a-zA-Z]*))";
        return pattern;
    }

    private static String getNumberpattern() {
        String patternTele = "((\\d{3} \\d{1} \\d{3} \\d{3} \\d{4})|\\d{5}.?\\d*|(\\d{3} \\d{3} \\d{3} \\d{4})|(\\d{3}-\\d{4})|(\\+\\d{0,2} ?\\(\\d{2,3}\\) ?\\d{3}-\\d{4})|(\\d?\\(\\d{3}\\)\\d{3}-\\d{4})|(\\+\\d{2} \\(\\d{2}\\) ))";
        return patternTele;
    }

    private static File savdataFile() {
        File userHomeDirectory = new File(System.getProperty("user.home"));
        File savedataFile = new File(userHomeDirectory, dataFileName);
        return savedataFile;
    }

    public static void main(String[] args) {


        String name, number;
        TreeMap<String, String> phoneBook;
        phoneBook = new TreeMap<String, String>();


        File savedataFile = savdataFile();

        if (!savedataFile.exists()) {
            System.out.println("No Phonebook file found.");
            System.out.println(" A new one will be created, if you add any entries.");
            System.out.println("File name:" + savedataFile.getAbsolutePath());
        } else {
            System.out.println("Reading phone directory data");
            try (Scanner scanner = new Scanner(savedataFile)) {
                while (scanner.hasNextLine()) {
                    String phoneEntry = scanner.nextLine();
                    int separatorPosition = phoneEntry.indexOf('%');
                    if (separatorPosition == -1)
                        throw new IOException("File is not a phonebook data file.");
                    name = phoneEntry.substring(0, separatorPosition);
                    number = phoneEntry.substring(separatorPosition + 1);
                    phoneBook.put(name, number);
                }
            } catch (IOException e) {
                System.out.println("Error in phone book data file.");
                System.out.println("File name:  " + savedataFile.getAbsolutePath());
                System.out.println("This program cannot continue.");
                System.exit(1);
            }
        }


        in = new Scanner(System.in);
        boolean changed = false;

        while (true) {
            System.out.println("\n Enter your command:");
            String cmd = in.nextLine().trim();

            String[] arguments = cmd.split(" ");


            if (arguments.length == 1 && !arguments[0].toUpperCase().equals("LIST") && !arguments[0].toUpperCase().equals("EXIT")) {
                System.out.println(" ----HELP SCREEN----");
                System.out.println("Add operation done using: ADD'<PERSON>'''<Phone #>'");
                System.out.println("Delete operation of person using: DELNAME '<Name>'");
                System.out.println("Delete operation of telephone using:DELNUM '<Phone #>'");
                System.out.println("List operation:LIST");
                break;
            }

            switch (arguments[0].toUpperCase()) {
                case "ADD":
                    try {
                        String personName = cmd.substring(ordinalIndexOf(cmd, "\"", 1) + 1, ordinalIndexOf(cmd, "\"", 2));
                        String telePhone = cmd.substring(ordinalIndexOf(cmd, "\"", 3) + 1, ordinalIndexOf(cmd, "\"", 4));

                        if (personName.matches(getNamepattern()) && telePhone.matches(getNumberpattern())) {
                            System.out.println("Your entry has been added in the directory");
                            phoneBook.put(personName, telePhone);
                            changed = true;
                        } else {
                            System.out.println("Incorrect format of name/phone");
                        }
                    } catch (StringIndexOutOfBoundsException e) {
                        System.exit(1);
                    }


                    break;

                case "DELNUM":
                    String argumment = cmd.substring(ordinalIndexOf(cmd, "\"", 1) + 1, ordinalIndexOf(cmd, "\"", 2));
                    argumment = argumment.replace("\"", "");
                    String phoneDel = argumment.trim();
                    boolean boolTel = phoneBook.values().remove(phoneDel);
                    if (boolTel == true) {
                        changed = true;
                        System.out.println("\nDIRECTORY ENTRY REMOVED FOR " + phoneDel);
                    } else {
                        System.out.println("\nSORRY, THERE IS NO ENTRY FOR " + phoneDel);
                    }


                    break;

                case "DELNAME":
                    String argument = cmd.substring(ordinalIndexOf(cmd, "\"", 1) + 1, ordinalIndexOf(cmd, "\"", 2));
                    argumment = argument.replace("\"", "");

                    String personNameDel = argumment.trim();
                    String nameDel = phoneBook.get(personNameDel);
                    if (nameDel == null)
                        System.out.println("\nSORRY, THERE IS NO ENTRY FOR " + personNameDel);
                    else {
                        phoneBook.remove(personNameDel);
                        changed = true;
                        System.out.println("\nDIRECTORY ENTRY REMOVED FOR " + personNameDel);
                    }
                    break;

                case "LIST":
                    System.out.print("\n LIST CASE \n");
                    for (Map.Entry<String, String> entry : phoneBook.entrySet())
                        System.out.println("   " + entry.getKey() + ": " + entry.getValue());
                    break;

                case "EXIT":
                    System.exit(0);
                    break;


                default:
                    System.out.println("\nInvalid Option.");
                    System.exit(1);

            }

            if (changed) {
                changed = false;
                System.out.println("saving changes in the directory " +
                        savedataFile.getAbsolutePath() + " ...");
                PrintWriter out;
                try {
                    out = new PrintWriter(new FileWriter(savedataFile));
                } catch (IOException e) {
                    System.out.println("ERROR: Can't open file");
                    return;
                }
                for (Map.Entry<String, String> entry : phoneBook.entrySet())
                    out.println(entry.getKey() + "%" + entry.getValue());
                out.flush();
                out.close();
                if (out.checkError())
                    System.out.println("ERROR: some error occurred while reading the file");
                else
                    System.out.println("Done.");
            }


        }
    }

}
